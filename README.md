# finalExam

#### 项目介绍
此项目用来作为期末考试!四个人一组，每组选出一个小组长，从给出的题目中选择一个，每两周做一次发表。期末上交程序。

#### 考试步骤
1. 自由分组：4人一组，选出小组长
2. 申请账号：每人以自己名字的拼音全拼（如果重名，可加后缀）作为用户名，在码云https://gitee.com申请一个账号，每个组长关注此项目
3. 选题：从下面列表中选择一个题目（十一放假前可自己选择题目，放假后必须从下表中选择）
4. 把程序源码的初始版本上传码云
5. 小组成员为代码做贡献，直至完成程序（包括程序的说明文档）
6. 在程序编写过程中，进行课堂讲演（包括，本阶段完成的工作，这些工作的原理是什么，碰到的问题是什么，下一个阶段计划怎么解决等）
7. 根据每组的程序完成情况，以及做的讲演，给出分数

#### 候选列表

1. Tensorflow object detection API 打造属于自己的物体检测模型（https://www.bilibili.com/video/av21539370/?p=1）
2. TensorFlow项目实战视频课程-人脸检测（https://www.bilibili.com/video/av20770105/?p=7）
3. TensorFlow项目实战-DQN让AI自己玩游戏（https://www.bilibili.com/video/av20109337/?p=1）
4. Tensorflow项目实战-DCGAN，程序自己生成图片（https://www.bilibili.com/video/av20033914）
5. 基于Tensorflow、树莓派的无人驾驶小车（https://www.bilibili.com/video/av20401916，https://github.com/hamuchiwa/AutoRCCar）
6. 深度学习TensorFlow框架（3）：Tensorflow项目实战-验证码识别（https://www.bilibili.com/video/av28571800，https://www.bilibili.com/video/av27533039）
7. 利用dlib进行人脸识别签到（https://github.com/ageitgey/face_recognition）
8. 利用dlib进行人脸识别签到2（http://dlib.net/）
9. 利用MTCNN进行人脸识别签到（https://github.com/AITTSMD/MTCNN-Tensorflow）
10. 利用openCV进行人脸识别签到（自行搜集资料）
11. 利用facenet进行人脸识别签到（https://github.com/davidsandberg/facenet，https://github.com/008karan/Face-recognition，https://github.com/satinder147/Attendance-using-Face）
12. 利用openface进行人脸识别签到（https://github.com/cmusatyalab/openface）
13. 利用tensorflow.js进行人脸识别签到（慎选，https://swift.ctolib.com/justadudewhohacks-face-api-js.html）
14. 利用faceai进行人脸识别签到（https://github.com/vipstone/faceai）
15. 物体识别（https://github.com/balancap/SSD-Tensorflow）
16. 物体识别（https://github.com/qqwweee/keras-yolo3）
17. 利用yolov3进行物体识别（https://github.com/AlexeyAB/darknet）

#### 说明

1. 建议用python语言编写，或者用java语言
2. 需要自己学习git工具的使用
3. 人脸签到大体的步骤：A、事先把每个人的人脸照片保存起来，B、从一张图片中把人脸都检测出来，每人生成一张人脸图片，C、生成的每张人脸图片再比对每个人保存的图片，选出相似度最大的那个人，如果相似度达到一个阈值，既认定图片是这个人。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 参考资料
1. tensorflow：https://github.com/tensorflow/models
2. 参考网站其它项目：https://github.com/tensorflow
3. 吴恩达深度学习笔记：https://github.com/fengdu78/deeplearning_ai_books
4. 吴恩达机器学习笔记：https://github.com/fengdu78/Coursera-ML-AndrewNg-Notes
5. dlib官网：http://dlib.net/
6. 人脸识别的原理：https://zhuanlan.zhihu.com/p/24567586

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)